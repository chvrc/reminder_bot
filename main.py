from database import create_db
from aiogram.utils import executor
from load_bot import bot, loop, set_default_commands
import asyncio

from menu import scheduler, test_func


async def on_shutdown(dp):
    await bot.close()


async def on_startup(dispatcher):
    await set_default_commands(dispatcher)
    asyncio.create_task(test_func())
    asyncio.create_task(scheduler())
    create_db()


if __name__ == "__main__":
    from menu import dp
    executor.start_polling(dp, loop=loop,on_shutdown=on_shutdown, on_startup=on_startup)