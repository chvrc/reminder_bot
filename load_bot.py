import asyncio
import logging
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage


logging.basicConfig(level=logging.INFO)
API_TOKEN = "2040148538:AAEqfZuBbnqpcZFWXyg3hZEI_eIlmMS7Y7E"
ADMIN_ID = 170772210
ADMINS = [170772210, 1989427575]

loop = asyncio.get_event_loop()
bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage, loop=loop)


async def set_default_commands(dp):
    await dp.bot.set_my_commands(
        [
            types.BotCommand("show", "Активные напоминания"),
            types.BotCommand("stats", "Завершенные напоминания"),
            types.BotCommand("help", "Помощь по работе с ботом"),
        ]
    )