import sqlite3
import time
import datetime


def create_db():
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS tasks(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    task TEXT,
                    time INTEGER,
                    type TEXT);
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS completed_tasks(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    task TEXT,
                    time INTEGER,
                    type TEXT,
                    done INTEGER);
                    """)
    conn.commit()


def add_task(task, ttime, type):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                    INSERT OR IGNORE INTO tasks(task, time, type) VALUES(?, ?, ?);
                """, (task, ttime, type))
    cur.execute("""
                    SELECT * from tasks;
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def get_task(id):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM tasks
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def get_tasks():
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM tasks
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def get_filtered_tasks():
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM tasks
                ORDER BY time
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def edit_time(id, ttime):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE tasks
                SET time = ?
                WHERE id = ?
                """, (ttime, id))
    cur.execute("""
                    SELECT * from tasks
                    WHERE id = ?;
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def edit_task(id, task):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE tasks
                SET task = ?
                WHERE id = ?
                """, (task, id))
    cur.execute("""
                    SELECT * from tasks
                    WHERE id = ?;
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def del_task(id):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                    SELECT * from tasks
                    WHERE id = ?;
                """, (id, ))
    res = cur.fetchone()
    cur.execute("""DELETE FROM tasks
                    WHERE id = ?;
                    """, (id, ))
    conn.commit()
    return res


def get_completed_tasks():
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                    SELECT * FROM completed_tasks
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def del_completed_tasks():
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                    DELETE FROM completed_tasks
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def add_completed_task(task, ttype, done):
    ttime = int(time.time())
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO completed_tasks(task, time, type, done) VALUES(?, ?, ?, ?);
                """, (task, ttime, ttype, done))
    cur.execute("""SELECT * from completed_tasks;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


def time_to_datetime(id):
    conn = sqlite3.connect('reminder.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT time FROM tasks
                WHERE id='%s';
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return datetime.timedelta(seconds=res[0][0])
