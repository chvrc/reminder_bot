from aiogram.dispatcher.filters.state import State, StatesGroup


class Task(StatesGroup):
    forward_task = State()
    edit_task = State()
    time = State()
    type = State()