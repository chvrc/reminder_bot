import asyncio
import aioschedule
from database import add_completed_task, add_task, del_completed_tasks, del_task, edit_task, edit_time, get_completed_tasks, get_filtered_tasks, get_task, get_tasks
from aiogram.dispatcher.storage import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types.message import ContentType
from load_bot import ADMIN_ID, ADMINS, dp
from aiogram import types
import time
from states import Task
from word_to_datetime import wtotime


TASK_ID = 0
DELAY_TIME = 1


@dp.message_handler(Command('show'), user_id=ADMINS)
async def show_tasks(message: types.Message):
    db = get_filtered_tasks()
    if db:
        ans = '<u>Текущие напоминания</u>'
        for i in db:
            if i[3] is None:
                ans += f'\n➖<b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> /task_{i[0]}'
            else:
                ans += f'\n➖<b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> #{i[3]} /task_{i[0]}'
    else:
        ans = 'Активных напоминаний нет'
    await message.answer(ans, parse_mode='HTML')


@dp.message_handler(Command('help'), user_id=ADMINS)
async def show_tasks(message: types.Message):
    ans = '<b>Пример ввода напоминания:</b>\n' \
            '<i>Купить корм коту\nзавтра в 18.00</i>\n\n' \
            'Либо перешлите напоминание и следующим сообщением введите время\n\n' \
            '<b>Команды:</b>\n' \
            '➕<b>ок</b> - напоминание выполнено\n' \
            '➕<b>отмена</b> - напоминание отменено\n' \
            '➕<b>доп</b> - дополнить напоминание\n' \
            '➕<b>Число от 1 до 120</b> - перенести на <b>N</b> минут\n' \
            '➕Или введите время для переноса напоминания\n\n' \
            '<b>Пример ввода времени:</b>\n' \
            'через 5 минут, через 2 часа, завтра в 10.00, послезавтра в 12:00, 15 ноября в 11'
    await message.answer(ans, parse_mode='HTML')


@dp.message_handler(content_types=ContentType.CONTACT, user_id=ADMINS)
async def get_contact(message: types.Message):
    c = message.contact
    await dp.bot.send_contact(chat_id=message.from_user.id, phone_number=c.phone_number,
    first_name=c.first_name, last_name=c.last_name, vcard=c.vcard)


@dp.message_handler(lambda message: message.text.startswith('/task_'), user_id=ADMINS)
async def show_tasks(message: types.Message):
    global TASK_ID
    task = message.text[6:]
    if task.isdigit():
        TASK_ID = int(task)
        db_task = get_task(TASK_ID)
        if db_task:
            await message.answer('<u>Выбрано напоминание № {}</u>\n\n' \
                                f'➖<b>{db_task[1]}</b>\n🕓 <i>{time.ctime(db_task[2])}</i> /task_{db_task[0]}\n\n' \
                                '➕<b>ок</b> - напоминание выполнено\n' \
                                '➕<b>отмена</b> - напоминание отменено\n' \
                                '➕<b>доп</b> - дополнить напоминание\n' \
                                '➕<b>Число от 1 до 120</b> - перенести на N минут\n' \
                                '➕Или введите время для переноса напоминания'.format(task),
            parse_mode='HTML')
        else:
            await message.answer('Ошибка! Такого напоминания нет.')
    else:
        await message.answer('Ошибка! Такого напоминания нет.')


@dp.message_handler(Command('task'), user_id=ADMINS)
async def show_tasks(message: types.Message):
    global TASK_ID
    task = message.get_args()
    if task.isdigit():
        TASK_ID = int(task)
        db_task = get_task(TASK_ID)
        if db_task:
            await message.answer('<u>Выбрано напоминание № {}</u>\n\n' \
                                f'➖<b>{db_task[1]}</b>\n🕓 <i>{time.ctime(db_task[2])}</i> /task_{db_task[0]}\n\n' \
                                '➕<b>ок</b> - напоминание выполнено\n' \
                                '➕<b>отмена</b> - напоминание отменено\n' \
                                '➕<b>доп</b> - дополнить напоминание\n' \
                                '➕<b>Число от 1 до 120</b> - перенести на N минут\n' \
                                '➕Или введите время для переноса напоминания'.format(task),
            parse_mode='HTML')
        else:
            await message.answer('Ошибка! Такого напоминания нет.')
    else:
        await message.answer('Ошибка! Такого напоминания нет.')


@dp.message_handler(Command('delay'), state='*', user_id=ADMINS)
async def new_request(message: types.Message):
    global DELAY_TIME
    args = message.get_args()
    if args.isdigit():
        DELAY_TIME = int(args)
        await message.answer(f'Интервал изменен на <b>{DELAY_TIME}</b> мин.', parse_mode='HTML')
    else:
        await message.answer(f'Текущий интервал: <b>{DELAY_TIME}</b>', parse_mode='HTML')


@dp.message_handler(Command('stats'), user_id=ADMINS)
async def show_tasks(message: types.Message):
    db = get_completed_tasks()
    if db:
        ans = '<u>Статистика за день</u>'
        for i in db:
            if i[4] == 0:
                if i[3] is None:
                    ans += f'\n\n❌ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i>'
                else:
                    ans += f'\n\n❌ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> #{i[3]}'
            elif i[4] == 1:
                if i[3] is None:
                    ans += f'\n\n✅ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i>'
                else:
                    ans += f'\n\n✅ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> #{i[3]}'
            else:
                pass
    else:
        ans = 'Сегодня не было напоминаний'
    await message.answer(ans, parse_mode='HTML')


@dp.message_handler(lambda message: message.text.lower() == 'ок', user_id=ADMINS)  
async def task_ok(message: types.Message):
    try:
        db = del_task(TASK_ID)
        add_completed_task(db[1], db[3], 1)
        await message.answer(f'✅ <u>Напоминание <b>№ {db[0]}</b> выполнено</u>\n{db[1]}', parse_mode='HTML')
    except:
        await message.answer('Нет активного напоминания с таким номером')


@dp.message_handler(lambda message: message.text.lower() == 'отмена', user_id=ADMINS)  
async def task_ok(message: types.Message):
    try:
        db = del_task(TASK_ID)
        add_completed_task(db[1], db[3], 0)
        await message.answer(f'❌ <u>Напоминание <b>№ {db[0]}</b> отменено</u>\n<s>{db[1]}</s>', parse_mode='HTML')
    except:
        await message.answer('Нет активного напоминания с таким номером')


@dp.message_handler(lambda message: message.text.lower() == 'доп', user_id=ADMINS)  
async def task_ok(message: types.Message, state: FSMContext):
    try:
        db = get_task(TASK_ID)
        async with state.proxy() as data:
            data['task_id'] = TASK_ID
            data['task'] = db[1]
        await message.answer(f'Введите дополнительную информацию', parse_mode='HTML')
        await Task.edit_task.set()
    except:
        await message.answer('Нет активного напоминания с таким номером')


@dp.message_handler(state=Task.edit_task, user_id=ADMINS)
async def task_ok(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        db = edit_task(data['task_id'], f"{data['task']}\n{message.text}")
    if db[3] is None:
        ans = f'✏️ <u>Напоминание <b>№ {db[0]}</b> дополнено</u>\n{db[1]}'
    else:
        ans = f'✏️ <u>Напоминание <b>№ {db[0]}</b> дополнено</u>\n{db[1]}'

    await message.answer(ans, parse_mode='HTML')
    await state.finish()


@dp.message_handler(lambda message: message.text.isdigit(), user_id=ADMINS)
async def task_ok(message: types.Message):
    try:
        db = edit_time(TASK_ID, int(time.time() + int(message.text) * 60))
        await message.answer(f'<u>🕐 Напоминание <b>№ {db[0]}</b> перенесено на <i>{time.ctime(db[2])}</i></u>', parse_mode='HTML')
    except:
        await message.answer('Нет активного напоминания с таким номером')


@dp.message_handler(lambda message: wtotime(message.text), user_id=ADMINS)
async def task_ok(message: types.Message):
    try:
        db = edit_time(TASK_ID, wtotime(message.text))
        await message.answer(f'🕐 Напоминание <b>№ {db[0]}</b> перенесено на <i>{time.ctime(db[2])}</i>', parse_mode='HTML')
    except:
        await message.answer('Нет активного напоминания с таким номером')


@dp.message_handler(lambda message: message.is_forward(), user_id=ADMINS)  
async def send_welcome(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['forward_text'] = message.text
    await message.answer('Введите время напоминания или время и хештег через Enter\n\nДля отмены ввода напишите "нет"')
    await Task.forward_task.set()


@dp.message_handler(state=Task.forward_task, user_id=ADMINS)
async def add_forward_task(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        task = data['forward_text']
    text = message.text.split('\n')
    if text[-1].startswith('#'):
        if wtotime(text[-2]):
            ttime = wtotime(text[-2])
            ttype = text[-1][1:]
            db = await add_task(task, ttime, ttype)
            ans = f'ℹ️ <u>Создано напоминание</u>\n🕓 <i>{time.ctime(db[-1][2])}</i>\n📃 <b>{db[-1][1]}</b>\n#{db[-1][3]}'
            await state.finish()
        else:
            ans = '❗️ Неправильный формат ввода!\nВведите ещё раз или напишите "нет" для отмены ввода'
    elif wtotime(text[-1]):
        ttime = wtotime(text[-1])
        ttype = None
        db = add_task(task, ttime, ttype)
        ans = f'ℹ️ <u>Создано напоминание</u>\n🕓 <i>{time.ctime(db[-1][2])}</i>\n📃 <b>{db[-1][1]}</b>'
        await state.finish()
    elif message.text.lower() == 'нет':
        ans = 'Добавление напоминания отменено'
        await state.finish()
    else:
        ans = '❗️ Неправильный формат ввода!\nВведите ещё раз или напишите "нет" для отмены ввода'
    await message.answer(ans, parse_mode='HTML')


@dp.message_handler(user_id=ADMINS)  
async def send_welcome(message: types.Message, state: FSMContext):
    text = message.text.split('\n')
    if text[-1].startswith('#'):
        if wtotime(text[-2]):
            task = message.text[:-(len(text[-1]) + len(text[-2]) + 2)]
            ttime = wtotime(text[-2])
            ttype = text[-1][1:]
            db = add_task(task, ttime, ttype)
            ans = f'📃 <u>Создано напоминание</u>\n🕓 <i>{time.ctime(db[-1][2])}</i>\nℹ️ <b>{db[-1][1]}</b>\n#{db[-1][3]}'
        else:
            ans = '❗️ Неправильный формат ввода!'
    elif wtotime(text[-1]):
        task = message.text[:-len(text[-1]) - 1]
        ttime = wtotime(text[-1])
        ttype = None
        db = add_task(task, ttime, ttype)
        ans = f'📃 <u>Создано напоминание</u>\n🕓 <i>{time.ctime(db[-1][2])}</i>\nℹ️ <b>{db[-1][1]}</b>'
    else:
        async with state.proxy() as data:
            data['forward_text'] = message.text
        ans = 'Введите время напоминания или время и хештег через Enter\n\nДля отмены ввода напишите "нет"'
        await Task.forward_task.set()
    await message.answer(ans, parse_mode='HTML')


async def test_func():
    global TASK_ID
    global DELAY_TIME
    while True:
        db = get_tasks()
        for i in db:
            if i[2] - 1 <= int(time.time()) <= i[2] + 1:
                if i[3] == None:
                    ans = f'<b>{i[1]}</b>\n\n/task_{i[0]}'
                else:
                    ans = f'<b>{i[1]}\n#{i[3]}</b>\n\n/task_{i[0]}'
                await dp.bot.send_message(ADMIN_ID, ans, parse_mode='HTML')
                edit_time(i[0], int(time.time() + DELAY_TIME * 60))
                TASK_ID = i[0]
        await asyncio.sleep(1)


async def noon_print():
    db = get_completed_tasks()
    if db:
        ans = '<u>Статистика за день</u>'
        for i in db:
            if i[4] == 0:
                if i[3] is None:
                    ans += f'\n❌ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i>'
                else:
                    ans += f'\n❌ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> #{i[3]}'
            elif i[4] == 1:
                if i[3] is None:
                    ans += f'\n✅ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i>'
                else:
                    ans += f'\n✅ <b>{i[1]}</b>\n🕓 <i>{time.ctime(i[2])}</i> #{i[3]}'
            else:
                pass
    else:
        ans = 'Сегодня не было напоминаний'
    await del_completed_tasks()
    await dp.bot.send_message(ADMIN_ID, ans, parse_mode='HTML')


async def scheduler():
    aioschedule.every().day.at("21:00").do(noon_print)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)
