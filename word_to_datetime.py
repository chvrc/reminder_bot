import time
from datetime import datetime
import re


def ttime(i, dtime):
    return int(time.time() + int(i) * dtime)


def wtotime(word):
    try:
        word = word.lower()
        buff = word.split(' ')
        if word.startswith('через'):
            for i in buff:
                if 'минут' in word and i.isdigit():
                    return ttime(i, 60)
                elif 'час' in word and i.isdigit():
                    return ttime(i, 60 * 60)
                elif 'день' in word and i.isdigit():
                    return ttime(i, 60 * 60 * 24)
                elif 'дн' in word and i.isdigit():
                    return ttime(i, 60 * 60 * 24)
                elif 'недел' in word and i.isdigit():
                    return ttime(i, 60 * 60 * 24 * 7)
                elif 'месяц' in word and i.isdigit():
                    return ttime(i, 60 * 60 * 24 * 30)
        elif word.startswith('сегодня'):
            for i in buff:
                if i.isdigit():
                    if int(i) in [j for j in range(1, 25)]:
                        return int(datetime.now().replace(hour=int(i), minute=0, second=0, microsecond=0).timestamp())
                elif len(i.split('.')) > 1:
                    return int(datetime.now().replace(hour=int(i.split('.')[0]), minute=int(i.split('.')[1]), second=0, microsecond=0).timestamp())
                elif len(i.split(':')) > 1:
                    return int(datetime.now().replace(hour=int(i.split(':')[0]), minute=int(i.split(':')[1]), second=0, microsecond=0).timestamp())
                
        elif word.startswith('завтра'):
            for i in buff:
                if i.isdigit():
                    if int(i) in [j for j in range(1, 25)]:
                        return int(datetime.now().replace(hour=int(i), minute=0, second=0, microsecond=0).timestamp() + 60 * 60 * 24)
                elif len(i.split('.')) > 1:
                    return int(datetime.now().replace(hour=int(i.split('.')[0]), minute=int(i.split('.')[1]), second=0, microsecond=0).timestamp() + 60 * 60 * 24)
                elif len(i.split(':')) > 1:
                    return int(datetime.now().replace(hour=int(i.split(':')[0]), minute=int(i.split(':')[1]), second=0, microsecond=0).timestamp() + 60 * 60 * 24)
        elif word.startswith('послезавтра'):
            for i in buff:
                if i.isdigit():
                    if int(i) in [j for j in range(1, 25)]:
                        return int(datetime.now().replace(hour=int(i), minute=0, second=0, microsecond=0).timestamp() + 2 * 60 * 60 * 24)
                elif len(i.split('.')) > 1:
                    return int(datetime.now().replace(hour=int(i.split('.')[0]), minute=int(i.split('.')[1]), second=0, microsecond=0).timestamp() + 2 * 60 * 60 * 24)
                elif len(i.split(':')) > 1:
                    return int(datetime.now().replace(hour=int(i.split(':')[0]), minute=int(i.split(':')[1]), second=0, microsecond=0).timestamp() + 2 * 60 * 60 * 24)
        elif 'январ' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=1, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=1, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=1, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'феврал' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=2, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=2, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=2, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'март' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=3, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=3, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=3, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'апрел' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=4, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=4, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=4, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'ма' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=5, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=5, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=5, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'июн' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=6, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=6, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=6, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'июл' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=7, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=7, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=7, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'август' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=8, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=8, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=8, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'сентябр' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=9, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=9, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=9, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'октябр' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=10, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            if len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=10, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=10, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'ноябр' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=11, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=11, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=11, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        elif 'декабр' in word:
            if len(buff[3].split('.')) == 2:
                return int(datetime.now().replace(month=12, day=int(buff[0]), hour=int(buff[3].split('.')[0]), minute=int(buff[3].split('.')[1]), second=0, microsecond=0).timestamp())
            elif len(buff[3].split(':')) == 2:
                return int(datetime.now().replace(month=12, day=int(buff[0]), hour=int(buff[3].split(':')[0]), minute=int(buff[3].split(':')[1]), second=0, microsecond=0).timestamp())
            else:
                return int(datetime.now().replace(month=12, day=int(buff[0]), hour=int(buff[3]), minute=0, second=0, microsecond=0).timestamp())
        else:
            return False
    except:
        return False